'use strict';

// Declare app level module which depends on filters, and services
//express-angular-jade-mongo-base
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        
        $routeProvider.
            when('/', {
                templateUrl: 'partials/index',
                controller: IndexCtrl
            }).
            when('/newSample', {
                templateUrl: 'partials/newSample',
                controller: AddSampleCtrl
            }).
            // when('/editPost/:id', {
            //     templateUrl: 'partials/editPost',
            //     controller: EditPostCtrl
            // }).
            when('/deleteSample/:id', {
                templateUrl: 'partials/index',
                controller: DeleteSampleCtrl
            }).
            otherwise({
                redirectTo: '/'
            });

            $locationProvider.html5Mode(true);

  }]);