'use strict';

/* Controllers */
function IndexCtrl($scope, $http) {
    $scope.samples = [];

    $http.get('/api/samples').
        success(function(data, status, headers, config) {
            $scope.samples = data;
        });

    $scope.deleteSample = function (id) {
        $http.delete('/api/delete/' + id).
            success(function(data) {
                //Iterate through samples and push the ones that don't match the id
                var oldSamples = $scope.samples;
                $scope.samples = [];
                angular.forEach(oldSamples, function(sample) {
                    if (sample._id != id) $scope.samples.push(sample);
                });
            }).
            error(function(err, status) {
                console.log('Status: ' + status);
                console.log(err);
            });
    };
}

function AddSampleCtrl($scope, $http, $location) {
    $scope.form = {};
    $scope.submitSample = function () {
        $http.post('/api/newSample', $scope.form).
            success(function(data) {
                $location.path('/');
            }).
            error(function(err, status) {
                console.log('Status: ' + status);
                console.log(err);
            });
    };
}

// function EditPostCtrl($scope, $http, $location, $routeParams) {
//     $scope.form = {};
//     $http.get('/api/post/' + $routeParams.id).
//         success(function(data) {
//             $scope.form = data.post;
//         });

//     $scope.editPost = function () {
//         $http.put('/api/post/' + $routeParams.id, $scope.form).
//             success(function(data) {
//                 $location.url('/readPost/' + $routeParams.id);
//             });
//     };
// }

function DeleteSampleCtrl($scope, $http, $location, $routeParams) {
    $http.get('/api/sample/' + $routeParams.id).
        success(function(data) {
            $scope.sample = data;
        }).
        error(function(err, status) {
                console.log('Status: ' + status);
                console.log(err);
        });

    $scope.home = function () {
        $location.url('/');
    };
}