/**
 * Module dependencies.
 */
var express = require('express')
  , api = require('./routes/api')
  , routes = require('./routes')
  , environment = require('./environment')
  , MongoStore = require('connect-mongo')(express)
  , mongoose = require('mongoose');


var app = module.exports = express();

// Configuration
var conf;

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  conf = environment.env('development');
});

app.configure('production', function(){
  app.use(express.errorHandler());
  conf = environment.env('production');
});

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());

  app.use(express.cookieParser());
  app.use(express.session({
    secret: conf.secret,
    maxAge: new Date(Date.now() + 3600000),
    store: new MongoStore(conf.db)
  }));

  app.use(express.static(__dirname + '/public'));
  app.use(app.router);
});

// Routes
var dbUrl = environment.db(conf);
api.init(dbUrl);

app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

app.get('/api/samples', api.samples);
app.get('/api/sample/:id', api.sample);
app.post('/api/newSample', api.newSample);
app.delete('/api/delete/:id', api.deleteSample);

// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
});
