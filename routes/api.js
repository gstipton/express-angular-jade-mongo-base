var mongoose = require('mongoose');
var db;
var Schema;
var SampleSchema;

exports.init = function(dbUrl) {
	db = mongoose.connect(dbUrl);
	Schema = mongoose.Schema;
	SampleSchema = new Schema({
		  name				: String
		, data				: String
	});
	mongoose.model('Sample', SampleSchema);
}

// GET
exports.samples = function(req, res) {
	var Sample = mongoose.model('Sample');
	var query = Sample.find({});
	query.exec(function (err, samples) {
		if (err) {
			console.log(err);
		}
		res.json(samples);
	});
};

exports.sample = function(req, res) {
	var Sample = mongoose.model('Sample');
	var query = Sample.findById(req.params.id, function(err, sample) {
		if (err) {
			console.log(err);
		}
		res.json(sample);
	});
}

// POST
exports.newSample = function(req, res) {
	var Sample = mongoose.model('Sample');
	var sample = new Sample();
	sample.name = req.body.name;
	sample.data = req.body.data;
	sample.save(function (err) {
		if (err) {
			console.log(err);
			console.trace();
			res.json(false);
		} else {
			res.json(true);
		}

	});
}

// DELETE
exports.deleteSample = function(req, res) {
	var Sample = mongoose.model('Sample');
	var id = req.params.id;
	Sample.findById(id).remove();
	res.json(true);
}