exports.appName = "express-angular-jade-mongo-base";

var environment;

exports.env = function setEnv(env) {
	environment = env;
	if (env == 'development') {
		// url = 'mongodb://localhost:27017/express-base';
		return devConf;
	} else if (env == 'production') {
		// url = 'TBD';
		return prodConf;
	}
};

exports.db = function db(conf) {
  return buildUrl(conf);
}

function buildUrl(conf) {
  var dbUrl = 'mongodb://';
  dbUrl += conf.db.username+':'+conf.db.password+'@';
  dbUrl += conf.db.host+':'+conf.db.port;
  dbUrl += '/' + conf.db.db;
  return dbUrl;
}

var devConf = {
  db: {
    db: 'test',
    host: 'localhost',
    port: 27017,
    username: '', // optional
    password: '', // optional
  },
  secret: '076ee61d63aa10a125ea872411e433b9'
};

var prodConf = {
  db: {
    db: '',
    host: '',
    port: '',
    username: '', // optional
    password: '', // optional
    collection: '' // optional, default: sessions
  },
  secret: '076ee61d63aa10a125ea872411e433b9'
};