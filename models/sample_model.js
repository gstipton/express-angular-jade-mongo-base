module.exports = function (mongoose) {

	var modelObject = {};

	var Schema = mongoose.Schema;

	var SampleSchema = new Schema({
		  name				: String
		, data				: String
	});

	modelObject.SampleSchema = SampleSchema;
	modelObject.Sample = mongoose.model('Sample', SampleSchema);

	return modelObject;
};